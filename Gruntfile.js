module.exports = function(grunt) {

	// Project configuration.
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		copy: {
			main: {
				files: [
					// JavaScript Libraries --------------------------------------------------------
					// bootstrap
					{expand: true, flatten: true, src: ['bower_components/bootstrap/dist/js/bootstrap.min.js'], dest: 'scripts/Bootstrap/', filter: 'isFile'},

					// noty
					{expand: true, flatten: true, src: ['bower_components/noty/js/noty/packaged/*'], dest: 'scripts/noty/', filter: 'isFile'},

					// jquery
					{expand: true, flatten: true, src: ['bower_components/jquery/dist/jquery.min.js'], dest: 'scripts/', filter: 'isFile'},

					// knockout
					{src: ['bower_components/knockout/dist/knockout.js'], dest: 'scripts/knockout.min.js', filter: 'isFile'},

					// knockout-mapping
					{src: ['bower_components/knockout-mapping/build/output/knockout.mapping-latest.js'], dest: 'scripts/knockout.mapping.min.js', filter: 'isFile'},

					// requirejs
					{expand: true, flatten: true, src: ['bower_components/requirejs/require.js'], dest: 'scripts/', filter: 'isFile'},

					// CSS Libraries --------------------------------------------------------
					// bootstrap
					{expand: true, flatten: true, src: ['bower_components/bootstrap/dist/css/bootstrap-theme.min.css'], dest: 'content/styles', filter: 'isFile'},
					{expand: true, flatten: true, src: ['bower_components/bootstrap/dist/css/bootstrap.min.css'], dest: 'content/styles', filter: 'isFile'},
					{expand: true, flatten: true, src: ['bower_components/bootstrap/fonts/*.{woff,woff2}'], dest: 'content/fonts/', filter: 'isFile'},

					// font-awesome
					{expand: true, flatten: true, src: ['bower_components/font-awesome/css/font-awesome.min.css'], dest: 'content/styles', filter: 'isFile'},
					{expand: true, flatten: true, src: ['bower_components/font-awesome/fonts/*.{woff,woff2}'], dest: 'content/fonts/', filter: 'isFile'},
				],
			},
			dist: {
				files: [
					{expand: true, src: ['scripts/**'], dest: 'dist/', filter: 'isFile'},

					{expand: true, src: ['content/fonts/*'], dest: 'dist/', filter: 'isFile'},
					{expand: true, src: ['content/styles/**'], dest: 'dist/', filter: 'isFile'},
					{expand: true, src: ['content/images/*'], dest: 'dist/', filter: 'isFile'},

					{expand: true, src: ['app/**'], dest: 'dist/', filter: 'isFile'},

					{expand: true, flatten: true, src: ['manifest.json'], dest: 'dist', filter: 'isFile'},
				],
			}
		},
		cssmin: {

				options: {
					shorthandCompacting: false,
					roundingPrecision: -1
				},
				dist: {
					files: {
						'dist/App/popup/Content/Styles/style.min.css': ['app/popup/content/styles/*.css']
					}
				}
		}
	});

	grunt.loadNpmTasks('grunt-contrib-copy');
	grunt.loadNpmTasks('grunt-contrib-cssmin');

	// Default task(s).
	grunt.registerTask('default', ['copy:main', 'copy:dist', 'cssmin:dist']);

	grunt.registerTask('dist', ['copy:dist']);
};