(function () {
	function getScriptsVersion() {
		var script = document.querySelector('script[src*="main.js"]') || document.querySelector('script[src*="main-built"]');
		if (!script) {
			return '';
		}
		var indexOfVersion = script.src.indexOf('=');
		if (indexOfVersion === -1) {
			return '';
		}
		return script.src.substring(indexOfVersion + 1);
	}

	requirejs.config({
		waitSeconds: 120,
		baseUrl: './',
		paths: {
			'jquery': '../../Scripts/jquery.min',
			'knockout': '../../Scripts/knockout.min',
			'knockout-mapping': '../../Scripts/knockout.mapping.min',

			'bootstrap': '../../Scripts/Bootstrap/bootstrap.min',

			'modules': '../modules',
			'enums': '../modules/enums',
			'dataContext': '../modules/dataContext',
			'knockoutBindings': '../modules/knockoutBindings',

			'noty': '../../Scripts/noty/jquery.noty.packaged'
		},
		shim: {
			'knockout': {
				deps: ['jquery']
			},
			'bootstrap': {
				deps: ['jquery']
			},
			'noty': {
				deps: ['jquery']
			}
		}
	});

	// Do not merge this configuration with main configuration function above. The first configuration is for scripts minification
	requirejs.config({
		urlArgs: 'v=' + getScriptsVersion()
	});

	require(['knockout',
			'popup',
			'bootstrap',
			'knockoutBindings/textFormatted',
			'noty'],
		function (ko, popup) {
			ko.components.register('items-editor', {
				viewModel: { require: 'components/viewmodels/itemsEditor' },
				template: { require: 'text!components/views/itemsEditor.html' }
			});

			$(function(){
				popup.start();
			});

			$.noty.defaults = {
				layout: {name: 'top'},
				theme: 'defaultTheme', // or 'relax'
				type: 'alert',
				text: '', // can be html or string
				dismissQueue: true, // If you want to use queue feature set this true
				template: '<div class="noty_message"><span class="noty_text"></span><div class="noty_close"></div></div>',
				animation: {
					open: {height: 'toggle'}, // or Animate.css class names like: 'animated bounceInLeft'
					close: {height: 'toggle'}, // or Animate.css class names like: 'animated bounceOutLeft'
					easing: 'swing',
					speed: 500 // opening & closing animation speed
				},
				timeout: false, // delay for closing event. Set false for sticky notifications
				force: false, // adds notification to the beginning of queue when set to true
				modal: false,
				maxVisible: 5, // you can set max visible notification for dismissQueue true option,
				killer: false, // for close all notifications before show
				closeWith: ['click'], // ['click', 'button', 'hover', 'backdrop'] // backdrop click will close all notifications
				callback: {
					onShow: function() {},
					afterShow: function() {},
					onClose: function() {},
					afterClose: function() {},
					onCloseClick: function() {},
				},
				buttons: false // an array of buttons
			};
		});
})();
