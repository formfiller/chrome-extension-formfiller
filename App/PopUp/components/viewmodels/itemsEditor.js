define(['knockout', 'dataContext'],
function(ko, dataContext) {
	function ViewModel(params){
		var self = this;

		this.currentItem = params.currentItem;
		this.values = ko.observableArray([]);

		this.currentItem.subscribe(function(){
			if(self.currentItem && self.currentItem() && self.currentItem().values) {
				self.values(self.currentItem().values());
			} else {
				self.values([]);
			}
		});

		this.save = function() {
			if(self.currentItem && self.currentItem() && self.currentItem().values) {
				self.currentItem().values(self.values);
				dataContext.storedData.update(self.currentItem());
			}
		};

		this.revert = function() {

		};
	}

	return ViewModel;
});
