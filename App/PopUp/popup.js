define(['knockout', 'popupModel', 'dataContext'], function(ko, PopUpModel, dataContext){
	self = this;

	this.viewModel = new PopUpModel();

	this.start = function(){
		console.log('popup started');

		dataContext.initialize();

		ko.applyBindings(self.viewModel);
	};

	return self;
});
