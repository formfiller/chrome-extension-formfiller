define(['jquery',
		'knockout',
		'knockout-mapping',
		'dataContext',
		'enums',
		'modules/urlHelper',
		'modules/contentCommunicator',
		'modules/helpers',
		'modules/notifications',
		'modules/sliderAnimation',
		'modules/analytics'],
function($, ko, mapping, dataContext, enums, urlHelper, contentCommunicator, helpers, notifications, sliderAnimation, _gag) {
	function PopUpModel() {
		var CONTROL_TYPES = ['text', 'checkbox', 'select', 'textarea'];

		var self = this;

		var currentTab = ko.observable();
		chrome.tabs.query({active: true, currentWindow: true}, function (tabs) {
			currentTab(tabs[0]);
		});

		this.data = ko.observableArray([]);
		dataContext.storedData.data.subscribe(function(){
			var res = [];
			if(currentTab()) {
				var res = dataContext.storedData.data();
				var currentDomain = urlHelper.parseURL(currentTab().url).domain;
				res = res.filter(function (item) {
					return item.domain().toLowerCase() === currentDomain.toLowerCase();
				});
				res.sort(function (first, second) {
					return new Date(second.date()).getTime() - new Date(first.date()).getTime();
				});
			}

			self.data(res);
		}, null, "arrayChange");

		this.mappings = ko.observableArray([]);
		dataContext.storedMappings.data.subscribe(function(){
			var res = [];
			if(currentTab()) {
				var currentMapping = dataContext.storedMappings.data();
				var currentDomain = urlHelper.parseURL(currentTab().url).domain;
				currentMapping = currentMapping.filter(function (item) {
					return item.domain().toLowerCase() === currentDomain.toLowerCase();
				});

				if(currentMapping && currentMapping[0] && currentMapping[0].values) {
					res = currentMapping[0].values();
				}
			}

			self.mappings(res);
		}, null, "arrayChange");

		this.editedItem = ko.observable();

		this.isControlsMapChanged = ko.observable(false);

		this.basicMapAndSave = function () {
			_gag.push(['_trackEvent', 'basicMapAndSave', 'clicked']);
			console.log("basicMapAndSave");

			contentCommunicator.post(
				enums.Commands.GET_DATA,
				{ types: CONTROL_TYPES },
				function handler(response, tab) {
					if (response) {
						var identified = helpers.removeNotIdentified(response);
						var unique = helpers.removeDuplicated(identified);
						if(unique.length !== 0) {
							dataContext.storedData.push({
								values: unique,
								date: new Date(),
								url: tab.url,
								domain: urlHelper.parseURL(tab.url).domain,
								name: ''
							});

							notifications.success('Controls values Saved !', '#basicMappingNotificationsContainer');
						} else {
							notifications.warning('No data came from the page !', '#basicMappingNotificationsContainer');
						}
					} else {
						notifications.warning('No data came from the page ! <br/> (possibly no input controls)', '#basicMappingNotificationsContainer');
					}
				});
		};

		this.basicRestore = function (item) {
			_gag.push(['_trackEvent', 'basicRestore', 'clicked']);
			console.log("basicRestore");

			if (item) {
				contentCommunicator.post(
					enums.Commands.RESTORE_DATA,
					mapping.toJS(item.values),
					function handler(response) {
						//console.log(response);
					});
			}
		};

		this.basicEdit = function (item) {
			_gag.push(['_trackEvent', 'basicEdit', 'clicked']);
			console.log("basicEdit");

			if (item) {
				self.editedItem(item);
				sliderAnimation.openDetails('#container', '#main-accordion-container', '#edit-items-container');
			}
		};

		this.backFromEdit = function (item) {
			_gag.push(['_trackEvent', 'backFromEdit', 'clicked']);
			console.log("backFromEdit");

			if (item) {
				sliderAnimation.backToMain('#container', '#main-accordion-container', '#edit-items-container');
			}
		};

		this.basicDelete = function (item) {
			_gag.push(['_trackEvent', 'basicDelete', 'clicked']);
			console.log("basicDelete");

			dataContext.storedData.remove(item);
		};

		this.changedMappingAvailability = function() {
			self.isControlsMapChanged(true);
		};

		this.changedDataScopeName = function(){
			dataContext.storedData.update();
		};

		this.openOptions = function(){
			chrome.runtime.openOptionsPage();
		};

		this.hide = function(){
			$('#test1').addClass('animated fadeOutLeft');
			$('#test2').addClass('animated fadeInRight');
		};

		this.mapControls = function(){
			_gag.push(['_trackEvent', 'mapControls', 'clicked']);

			contentCommunicator.post(
				enums.Commands.GET_DATA,
				{ types: CONTROL_TYPES },
				function handler(response, tab) {
					if (response) {
						var identified = helpers.removeNotIdentified(response);
						var unique = helpers.removeDuplicated(identified);

						if(unique.length > 0) {
							ko.utils.arrayForEach(unique, function (item) {
								item.status = enums.MappingStatuses.ACTIVE;
							});

							dataContext.storedMappings.replace({
								values: unique,
								date: new Date(),
								url: tab.url,
								domain: urlHelper.parseURL(tab.url).domain
							});

							notifications.success('Controls have been Mapped !', '#advanceUsageNotificationsContainer');
						} else {
							notifications.warning('No controls came from the page !', '#advanceUsageNotificationsContainer');
						}
					} else {
						notifications.warning('No controls came from the page ! <br/> (possibly no inputs on it)', '#advanceUsageNotificationsContainer');
					}
				});
		};

		this.saveControlsMapping = function(){
			_gag.push(['_trackEvent', 'saveControlsMapping', 'clicked']);

			dataContext.storedMappings.update();
			self.isControlsMapChanged(false);

			notifications.success('Controls mapping Saved !' ,'#advanceUsageNotificationsContainer');
		};

		this.saveData = function(){
			_gag.push(['_trackEvent', 'saveData', 'clicked']);

			contentCommunicator.post(
				enums.Commands.GET_DATA,
				{ types: CONTROL_TYPES },
				function handler(response, tab) {
					if (response) {
						var unique = helpers.removeDuplicated(response);

						unique = ko.utils.arrayFilter(unique, function(item){
							if(item.id){
								return !!ko.utils.arrayFirst(self.mappings(), function(mapping){
									return mapping.id() === item.id && mapping.status() === enums.MappingStatuses.ACTIVE;
								});
							} else if(item.name){
								return !!ko.utils.arrayFirst(self.mappings(), function(mapping){
									return mapping.name() === item.name && mapping.status() === enums.MappingStatuses.ACTIVE;
								});
							}
							return false;
						});

						dataContext.storedData.push({values: unique, date: new Date(), url: tab.url, domain: urlHelper.parseURL(tab.url).domain, name: ''});

						$('.panel-collapse.in').collapse('hide');
						$('#basicMappingPanel').collapse('show');

						notifications.success('Controls values Saved !' ,'#basicMappingNotificationsContainer');
					}
				});
		};

		this.clearCache = function(){
			_gag.push(['_trackEvent', 'clearCache', 'clicked']);

			dataContext.storedData.clear();
			dataContext.storedMappings.clear();
		};
	}

	return PopUpModel;
});