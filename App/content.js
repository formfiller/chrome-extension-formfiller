var Commands = Object.freeze({
	GET_DATA: 'getData',
	RESTORE_DATA: 'restoreData',
	GET_MAPPING: 'getMapping'
});

chrome.runtime.onConnect.addListener(function(port) {
	port.onMessage.addListener(function(msg) {
		port.postMessage({counter: msg.counter+1});
	});
});

chrome.extension.onRequest.addListener(
	function(request, sender, sendResponse) {
		if(request.command === Commands.GET_DATA) {
			var data = [];

			// Save values of inputs elements
			var inputs = document.getElementsByTagName('input');
			for (i = 0; i < inputs.length; ++i) {
				var currentInput = inputs[i];
				// check if current input type should be saved
				if (request.data.types.indexOf(currentInput.type) >= 0) {
					// separate logic for checkbox elements
					if(currentInput.type === 'checkbox'){
						data.push({id: currentInput.id, name: currentInput.name, val: currentInput.checked, type: currentInput.type})
					} else {
						data.push({id: currentInput.id,	name: currentInput.name, val: currentInput.value, type: currentInput.type});
					}
				}
			}

			// Save values of select elements
			if (request.data.types.indexOf('select') >= 0) {
				var selects = document.getElementsByTagName('select');
				for (i = 0; i < selects.length; ++i) {
					var currentSelect = selects[i];
					data.push({	id: currentSelect.id, name: currentSelect.name, val: currentSelect.options[currentSelect.selectedIndex].value, index: currentSelect.selectedIndex});
				}
			}

			// Save values of texarea elements
			if (request.data.types.indexOf('textarea') >= 0) {
				var textareas = document.getElementsByTagName('textarea');
				for (i = 0; i < textareas.length; ++i) {
					var currentTextarea = textareas[i];
					data.push({	id: currentTextarea.id, name: currentTextarea.name, val: currentTextarea.value });
				}
			}

			sendResponse(data);
		} else if(request.command === Commands.RESTORE_DATA && request.data){
			request.data.forEach(function(item){
				console.log(item);
				var element = undefined;
				if(item.id){
					element = document.getElementById(item.id);
				} else if(item.name){
					element = document.getElementsByName(item.name)[0];
				}

				if(element){
					if(item.type === 'checkbox'){
						element.checked = item.val;
					} else {
						element.value = item.val;
					}
				}
			});
		}
	});
