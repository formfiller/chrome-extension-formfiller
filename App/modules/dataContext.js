define(['knockout', 'knockout-mapping'],function (ko, mapping) {
	function Repository(key){
		var that = this;

		this.key = key;
		this.data = ko.observableArray([]);

		this.initialize = function (){
			chrome.storage.local.get(that.key, function (data){
				if(data && data[that.key]) {
					//console.log(data[that.key]);
					ko.utils.arrayForEach(JSON.parse(data[that.key]), function (item){
						that.data.push(mapping.fromJS(item));
					});
				}
			});
		};

		this.update = function(){
			var savedObject = {};
			// todo: think about ko.utils method
			savedObject[that.key]= JSON.stringify(mapping.toJS(that.data));
			chrome.storage.local.set(savedObject);
		};

		this.pushOrUpdate = function(item){
			var changedItems = that.data.remove(function(currentItem) {
				return currentItem.id === item.id;
			});
			that.data.push(item);

			that.update();
		};

		this.push = function(item){
			item.id = Math.random();

			that.data.push(mapping.fromJS(item));
			that.update();
		};

		this.replace = function(item){
			that.clear();
			that.push(item);
		};

		this.remove = function(item){
			that.data.remove(function(storedItem) { return storedItem.id() === item.id()});
			that.update();
		};

		this.clear = function(){
			that.data([]);
			chrome.storage.local.remove(that.key);
		};
	}

	var self = this;

	this.storedData = new Repository('storedData');
	this.storedMappings = new Repository('storedMappings');

	this.initialize = function(){
		self.storedData.initialize();
		self.storedMappings.initialize();
	};

	return self;
});
