define(['knockout', 'modules/formatting'],
	function (ko, formatting) {
		/**
		 * Read-only (one-way) binding to easily format numeric/date fields. This binding uses our own formatting engine.
		 * Supports knockout virtual elements
		 * Could be used for any html attribute formatting
		 * Examples:
		 *	<span data-bind="textFormatted: dateValueOrObservable, format: { type: 'date', dateFormat: 'T dd yyyy' }" />
		 *	<span data-bind="textFormatted: currencyValueOrObservable, format: 'currency'" />
		 *	<!-- ko textFormatted: someValueOrObservable, format: 'percentage' --><!-- /ko -->
		 * Available options:
		 *	'currency';
		 *	'percentage' ( format: { type: 'percentage', signed: true } or format: 'percentage' );
		 *	'volume';
		 *	'date' (format: { type: 'date', dateFormat: 'T dd yyyy' }, or format: 'date');
		 'string'
		 */
		function declareFormattedBinding(name, bindingHandler) {
			ko.bindingHandlers[name] = {
				init: bindingHandler.init,
				update: function (element, valueAccessor, allBindings, viewModel) {
					var value = ko.utils.unwrapObservable(valueAccessor());
					var options = allBindings().format || {};
					var formattedValue = formatting.customFormat(value, options);

					function customValueAccessor() {
						return formattedValue;
					}
					bindingHandler.update.call(this, element, customValueAccessor, allBindings, viewModel);
				}
			};

			ko.virtualElements.allowedBindings[name]= true;
		}

		declareFormattedBinding('textFormatted', ko.bindingHandlers.text);
		if (ko.bindingHandlers.title) {
			declareFormattedBinding('titleFormatted', ko.bindingHandlers.title);
		}

		return ko;
	});