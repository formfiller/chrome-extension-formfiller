define(['jquery'], function($){
	var self = this;

	var Types = Object.freeze({
		MOVE_TO_RIGHT : 'moveToRight', //visible from 1 to 0 0 <- 1
		MOVE_TO_LEFT : 'moveToLeft', //visible from 0 to 1 0 -> 1
	});

	function slideContainer(container, leftPanel, rightPanel, type){
		var $container = $(container);
		var $firstPanel; //will be visible
		var $secondPanel; // will be hided

		if(type === Types.MOVE_TO_RIGHT){
			$firstPanel = $(leftPanel);
			$secondPanel = $(rightPanel);

			$container.animate({left: '0px'}, 1000);
		} else {
			$firstPanel = $(rightPanel);
			$secondPanel = $(leftPanel);

			$container.animate({left: '-500px'}, 1000);
		}

		$firstPanel.css('visibility', 'hidden');
		$firstPanel.css('height', '');

		var firstHeight = $firstPanel.outerHeight();
		var secondHeight = $secondPanel.outerHeight();

		$firstPanel.css('height', secondHeight);
		$firstPanel.css('visibility', '');

		$firstPanel.animate({height: firstHeight}, 1000);
		$secondPanel.animate({height: firstHeight}, 1000);

		setTimeout(function(){
			$firstPanel.css('height', '');

			$secondPanel.css('visibility', 'hidden');
			$secondPanel.css('height', '1px');
		}, 1100);
	}

	this.openDetails = function (container, leftPanel, rightPanel) {
		slideContainer(container, leftPanel, rightPanel, Types.MOVE_TO_LEFT);
	};

	this.backToMain = function (container, leftPanel, rightPanel) {
		slideContainer(container, leftPanel, rightPanel, Types.MOVE_TO_RIGHT);
	};

	return self;
});
