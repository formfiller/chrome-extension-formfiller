define([],function () {
		function notAvailableString() {
			return 'N/A';
		}

		var DEFAULT_DATE_FORMAT = 'T dd yyyy';
		var EXPIRY_KEY_DATE_FORMAT = 'yyyyMMdd';
		var DEFAULT_LIMIT_VALUE_FOR_FRACTIONS = 9999;
		var MARKET_TIME_ZONE = 'EST5EDT';

		// e.g. var fu = paddy(14, 5); // 00014
		// e.g. var bar = paddy(2, 4, '#'); // ###2
		function paddy(number, numberOfSignsInResult, padChar) {
			padChar = padChar != null ? padChar : '0';
			numberOfSignsInResult = numberOfSignsInResult != null ? numberOfSignsInResult : 2;
			var pad = new Array(1 + numberOfSignsInResult).join(padChar);
			return (pad + number).slice(-pad.length);
		}

		// converts e.g. 9575.7857 → 9,575.785,7
		function convertToNumberWithCommas(number) {
			number = number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
			return number;
		}

		// converts e.g. 9575.7857 → 9,576
		function toIntString(num) {
			var number = roundNumber(num, 0);
			number = convertToNumberWithCommas(number);
			return number;
		}

		// converts e.g. 9575.7857 → 9,576.79
		function toFractionalString(num, numberOfFractionalDigits) {
			if (numberOfFractionalDigits == null) {
				numberOfFractionalDigits = 2;
			}
			var number = roundNumber(num, numberOfFractionalDigits).toFixed(numberOfFractionalDigits);
			number = convertToNumberWithCommas(number);
			return number;
		}

		function toSignedFractionalString(num, signed, numberOfFractionalDigits) {
			var sign = '';

			if (signed) {
				if (num > 0) {
					sign = '+';
				} else if (num < 0) {
					sign = '–';
				} else {
					sign = '';
				}
			}
			var number = sign + toFractionalString(Math.abs(num), numberOfFractionalDigits);
			return number;
		}

		// converts e.g. 9575.7857 → $9,576
		function toIntCurrency(num) {
			var sign = num < 0
				? '–'
				: '';
			var number = sign + '$' + toIntString(Math.abs(num));
			return number;
		}

		// converts e.g. 9575.7857 → $9,575.79
		function toFractionalCurrency(num, numberOfFractionalDigits) {
			var sign = num < 0
				? '–'
				: '';
			var number = sign + '$' + toFractionalString(Math.abs(num), numberOfFractionalDigits);
			return number;
		}
		function toIntFractionalCurrency(num, numberOfFractionalDigits) {
			var mod = Math.abs(num) % 1;
			var result = mod < 0.01 || mod > 0.99 ? toIntCurrency(num) : toFractionalCurrency(num, numberOfFractionalDigits);
			return result;
		}
		/**
		 * @param {Number} num number to format
		 * @param {Number} limit below which (by absolute value) number will be converted as fractional currency.
		 * If number is greater than limit, it will be converted as integer currency.
		 *
		 * @example
		 * // returns → $9,575.79
		 * toLimitedFractionalCurrency(9575.7857, 9999)
		 *
		 * // returns → $99,575
		 * toLimitedFractionalCurrency(99575.7857, 9999)
		 */
		function toLimitedFractionalCurrency(num, limitValue) {
			limitValue = limitValue || DEFAULT_LIMIT_VALUE_FOR_FRACTIONS;
			return Math.abs(num) <= limitValue ? toFractionalCurrency(num) : toIntCurrency(num);
		}

		// converts e.g. 75.7857 → +75.79%
		function toPercentage(num, signed, numberOfFractionalDigits) {
			var sign = '';

			if (signed) {
				if (num > 0) {
					sign = '+';
				} else if (num < 0) {
					sign = '–';
				} else {
					sign = '';
				}
			}
			var number = sign + toFractionalString(Math.abs(num), numberOfFractionalDigits) + '%';
			return number;
		}

		function toFormattedVolume(num) {
			var billion = 1000000000;
			var million = 1000000;
			var billionSign = 'B';
			var millionSign = 'M';
			var result, doubleValue;

			if (num >= billion) {
				doubleValue = num / billion;
				doubleValue = toFractionalString(doubleValue);
				result = doubleValue + billionSign;
			} else if (num >= million) {
				doubleValue = num / million;
				doubleValue = toFractionalString(doubleValue);
				result = doubleValue + millionSign;
			} else {
				result = toFractionalString(num, 0);
			}
			return result;
		}

		function toExtendedChange(change, percentageChange, asIntFractional) {
			if (change == null || percentageChange == null) {
				return null;
			}
			var changeMark = change > 0
				? '+'
				: '';
			var transformFunction = asIntFractional ? toIntFractionalCurrency : toFractionalCurrency;
			var result = changeMark + transformFunction(change) + ' (' + toPercentage(percentageChange) + ')';
			return result;
		}

		function almostEqual(a, b, precission) {
			precission = precission || 1e-6;
			var result = Math.abs(a - b) < precission;
			return result;
		}

		function roundNumber(number, digits) {
			if (digits == null) {
				digits = 2;
			}
			var multiple = Math.pow(10, digits);
			var roundedNum = Math.round(number * multiple) / multiple;
			return roundedNum;
		}

		/**
		 * Rounds given number x to be multiple of number f
		 * @example
		 * //returns 2.50
		 * round(2.47, 0.05);
		 * //returns 2.45
		 * round(2.47, 0.05, true);
		 * @param {number} x Number to round
		 * @param {number} f Number to round
		 * @param {boolean=undefined} if not set - the value is rounded to nearest multiple of 'f'.
		 */
		function roundToMultiplier(x, f, roundDown) {
			var func = roundDown == null ? Math.round : roundDown ? Math.floor : Math.ceil;
			return f * func(x / f);
		}

		function aOrAn(num) {
			num = roundNumber(num, 0);
			while (num > 999) {
				num = roundNumber(num / 1000, 0);
			}

			if (num == 8 || num == 11 || num == 18 || (num >= 80 && num < 90)) {
				return 'an';
			} else {
				return 'a';
			}
		}

		/**
		 * Checks if @param d is a valid date object
		 */
		function isValidDateObject(d) {
			if (Object.prototype.toString.call(d) === '[object Date]') {
				return !isNaN(d.getTime());
			}
			else {
				return false;
			}
		}

		/**
		 * Converts a Date object from one time zone to another using the timezoneJS.Date library
		 * @param  {Date} dt                    Original JavaScript Date object, from the original time zone
		 * @param  {string} originalTimeZone    The original time zone
		 * @param  {string} targetTimeZone      The target time zone
		 * @return {timezoneJS.Date}            The date in the target timezone. This behaves the same as a native Date.
		 * @memberOf STX
		 */
		function convertTimeZone(dt, originalTimeZone, targetTimeZone) {
			// Convert from original timezone to local time
			var newDT = new STXThirdParty.timezoneJS.Date(dt.getFullYear(), dt.getMonth(), dt.getDate(), dt.getHours(), dt.getMinutes(), dt.getSeconds(), dt.getMilliseconds(), originalTimeZone);

			// Convert from local time to new timezone
			newDT.setTimezone(targetTimeZone);
			return newDT;
		}

		/*
		 * Returns Date object if date is valid date (string or in Date object).
		 * Applies additional time zone conversions if more than one argument is given
		 * NOTE: Javascript date returned still contain local timezone info.
		 * however date itself (hours, minutes) will be converted into targetTimeZone
		 * @param {Date|string} date date to parse and/or check
		 * @param {string} sourceTimeZone source date time zone. default - local time zone
		 * @param {string='EST5EDT'} targetTimeZone target time zone. Return value will be in this time zone
		 */
		function resolveDate(date, sourceTimeZone, targetTimeZone) {
			if (typeof (date) === 'string') {
				if (date.indexOf('Z', date.length - 1) === -1) {
					date += 'Z';
				}
				date = new Date(date);
				if (arguments.length !== 1) {
					date = throwTimezoneOffset(date);
				}
			}
			if (!isValidDateObject(date)) {
				return null;
			}
			if (arguments.length === 1) {
				return date;
			}
			var convertedDate = convertTimeZone(date, sourceTimeZone, targetTimeZone || MARKET_TIME_ZONE);
			var result = new Date(convertedDate.getFullYear(), convertedDate.getMonth(), convertedDate.getDate(),
				convertedDate.getHours(), convertedDate.getMinutes(), convertedDate.getSeconds(), convertedDate.getMilliseconds());
			return result;
		}

		function resolveExpiry(date) {
			var dateValue = resolveDate(date, MARKET_TIME_ZONE);
			if (dateValue) {
				if (dateValue.getDay() !== 6) {
					dateValue.setHours(16, 0, 0, 0);
				} else {
					dateValue.setHours(0, 0, 0, 0);
				}
			}
			return dateValue;
		}

		/**
		 * Returns date object in Market's time
		 */
		function getCurrentDateTime() {
			return resolveDate(new Date(), null);
		}

		function getCurrentDate() {
			var current = getCurrentDateTime();
			current.setHours(0, 0, 0, 0);
			return current;
		}

		/**
		 * Adds given number of days to start date and returns resulting date
		 * @param {number} numOfDays Number of days to add to start date
		 * @param {Date=<Current EST Date>} startDate Number of days to add to start date
		 */
		function getDateFromDaysInFuture(numOfDays, startDate) {
			var result = startDate ? new Date(startDate) : getCurrentDate();
			result.setDate(result.getDate() + numOfDays);
			return result;
		}

		function parseSungardExpiry(expiry) {
			if (typeof expiry != 'string') {
				return undefined;
			}
			var yyyy = parseInt(expiry.substr(0, 4), 10);
			var mm = parseInt(expiry.substr(4, 2), 10);
			var dd = parseInt(expiry.substr(6, 2), 10);

			var date = resolveExpiry(new Date(yyyy, mm - 1, dd));
			return date;
		}

		/**
		 * Throws out user's timezone from given date.
		 * Note: be careful while using this function. It modifies actual date and result could be wrong
		 */
		function throwTimezoneOffset(date, fromLocal) {
			date = resolveDate(date);
			if (!date) {
				return null;
			}
			var timezoneOffset = date.getTimezoneOffset() * 60000;
			if (fromLocal) {
				timezoneOffset *= -1;
			}
			var newDate = new Date(date.getTime() + timezoneOffset);
			return newDate;
		}

		/**
		 * Throws out user's timezone from given date, and converts it to ISO format so we can pass it via API
		 */
		function toUtcIsoDate(date) {
			var newDate = throwTimezoneOffset(date, true);
			return newDate && newDate.toISOString();
		}

		/**
		 * Convert local time to utc and format it as ISO string
		 */
		function convertStringToUtcIsoDate(date) {
			date = resolveDate(date);
			if (!date) {
				return null;
			}
			return date.toISOString();
		}

		function toExpiryKey(date) {
			var result = formatDate(date, EXPIRY_KEY_DATE_FORMAT);
			return result;
		}

		// (new Date()).format('yyyy-MM-dd hh:mm:ss.S') → 2006-07-02 08:09:04.423
		// (new Date()).format('yyyy-M-d h:m:s.S') → 2006-7-2 8:9:4.18
		function formatDate(date, dateFormat) {
			date = resolveDate(date);
			if (!date) {
				console.log('Invalid date object passed to formatDate function');
				return null;
			}

			dateFormat = dateFormat || DEFAULT_DATE_FORMAT;
			var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
			var o = {
				'M+': date.getMonth() + 1, // month
				'd+': date.getDate(), // date
				'h': date.getHours(), // hour
				'm+': date.getMinutes(), // minute
				's+': date.getSeconds(), // second
				'q+': Math.floor((date.getMonth() + 3) / 3), // season
				'S': date.getMilliseconds(), // millisecond
				'T': months[date.getMonth()] // month text
			};
			var month = date.getMonth() + 1;
			var dateNumber = date.getDate();

			if (dateFormat === 'yyyyMMdd') {
				var result = '' + date.getFullYear() + (month < 10 ? '0' : '') + month + (dateNumber < 10 ? '0' : '') + dateNumber;
				return result;
			}

			if (/(y+)/.test(dateFormat)) {
				dateFormat = dateFormat.replace(RegExp.$1, (date.getFullYear() + '').substr(4 - RegExp.$1.length));
			}

			for (var k in o) {
				if (!o.hasOwnProperty(k)) {
					continue;
				}
				if (new RegExp('(' + k + ')').test(dateFormat)) {
					dateFormat = dateFormat.replace(RegExp.$1, (RegExp.$1.length === 1)
						? o[k]
						: ('00' + o[k]).substr(('' + o[k]).length));
				}
			}
			return dateFormat;
		}

		function formatDateWithWeekNumber(date, dateFormat) {
			var formattedDate = formatDate(date, dateFormat);
			var weekNumber = getWeekNumber(date);
			var weekNumberString = '';
			if ((weekNumber !== 3) && (weekNumber >= 1) && (weekNumber <= 5)) {
				weekNumberString = weekNumber.toString();
			}
			return formattedDate + weekNumberString;
		}

		function getWeekNumber(date) {
			var dayDiff = 5 - date.getDay();
			var shiftedDate = new Date();
			shiftedDate.setDate(date.getDate() + dayDiff);
			var dayOfMonth = shiftedDate.getDate();
			var result = Math.ceil(dayOfMonth / 7);
			return result;
		}

		function getDatePart(date) {
			if (!isValidDateObject(date)) {
				return null;
			}
			return new Date(date.getFullYear(), date.getMonth(), date.getDate());
		}

		/**
		 * Compares date parts of dateTime objects.
		 * @returns 0 if dates are equal. 1 if first date is greater
		 */
		function compareOnlyDatePartFromDates(date1, date2) {
			var result = Math.sign(getDatePart(date1) - getDatePart(date2));
			return result;
		}

		function areDatesEqual(date1, date2) {
			var areEqual = compareOnlyDatePartFromDates(date1, date2) === 0;
			return areEqual;
		}

		function daysFromNow(date) {
			if (!date) {
				return 0;
			}
			var now = getCurrentDate().getTime();
			var diffMili = getDatePart(date).getTime() - now;
			var diff = diffMili / 1000 / 3600 / 24;
			diff = roundNumber(diff, 0);
			return diff;
		}

		function formatEarningsDate(earningsDate) {
			var days = daysFromNow(earningsDate);
			var strdate = formatDate(earningsDate, DEFAULT_DATE_FORMAT);
			var result = strdate + ' (' + (days < 0 ? 0 : days) + ')';

			return result;
		}

		function toNaString(value) {
			if (value == null) {
				return notAvailableString();
			}
			return value;
		}

		function customFormat(value, options) {
			var resultStr;
			var type = options.type;
			var signed = options.signed || false;
			var dateFormat = options.dateFormat;
			var decimalPlace = options.decimalPlace;

			if (typeof options === 'string') {
				type = options;
			}

			if (value == null) {
				return notAvailableString();
			}

			switch (type) {
				case 'intFractionalCurrency':
					resultStr = toIntFractionalCurrency(value, decimalPlace);
					break;
				case 'currency':
					resultStr = options.fractionLimited
						? toLimitedFractionalCurrency(value, options.fractionalLimit)
						: toFractionalCurrency(value, decimalPlace);
					break;
				case 'percentage':
					resultStr = toPercentage(value, signed, decimalPlace);
					break;
				case 'volume':
					resultStr = toFormattedVolume(value);
					break;
				case 'date':
					resultStr = formatDate(value, dateFormat);
					break;
				case 'earningsDate':
					resultStr = formatEarningsDate(value);
					break;
				case 'number':
					resultStr = roundNumber(value, decimalPlace);
					break;
				case 'fractional':
					resultStr = toSignedFractionalString(value, signed, decimalPlace);
					break;
				case 'pad':
					resultStr = paddy(value, decimalPlace);
					break;
				case 'aOrAn':
					resultStr = aOrAn(value);
					break;
				case 'ordinalIndicator':
					resultStr = ordinalIndicator(value);
					break;
				case 'capitaliseFirstLetter':
					resultStr = capitaliseFirstLetter(String(value));
					break;
				case 'string':
					resultStr = toNaString(value);
					break;
				default:
					resultStr = toFractionalString(value);
					break;
			}
			return resultStr;
		}

		// Formats string similar to printf() of String.Format()
		function format(str) {
			var args = Array.prototype.slice.call(arguments, 1);
			return str.replace(/{(\d+)}/g, function (match, number) {
				return typeof args[number] != 'undefined'
					? args[number]
					: match
					;
			});
		}

		function toPropertyName(str) {
			if (!str) {
				return str;
			}
			if (str[0] === str[0].toLowerCase()) {
				return str;
			}
			var result = str.toLowerCase();
			result = result.replace(/_([a-z])/g, function (g) { return g[1].toUpperCase(); });
			return result;
		}

		/**
		 * Formats string using object as replacement source.
		 * E.g. formatStr('{SOME_PROPERTY} continuation', {someProperty: 'Test'}) -> 'Test continuation'
		 * Casing inside brackets does not matter. E.g. {Test},{TEST},{TeSt} refers to the same property test
		 * Underscore followed by letter will be replaced with uppercase letter (camel casing)
		 * @param {string} str String to format
		 * @param data Object to take replacement values from
		 */
		function formatStr(str, data) {
			if (!data) {
				return str;
			}
			if (str == null) {
				return str;
			}

			var result = str;
			// Matches any string inside figure brackets
			var re = /\{(.*?)\}/g;
			var match;
			while ((match = re.exec(str)) != null) {
				var strToReplace = match[0];
				var propertyName = match[1];
				if (propertyName === '') {
					continue;
				}
				propertyName = toPropertyName(propertyName);
				var propertyValue = data[propertyName];
				if (typeof propertyValue === 'function') {
					propertyValue = propertyValue();
				}
				result = result.replace(strToReplace, propertyValue || '') || '';
			}
			return result.replace(/[\t ]+/g, ' ');
		}

		/**
		 * Extremely simplified pluralization for english words ONLY
		 */
		function pluralize(word, quantity) {
			if (Math.abs(quantity) <= 1) {
				return word;
			}

			function restoreCase(word, token) {
				if (word === word.toUpperCase()) {
					return token.toUpperCase();
				}

				// Title cased words. E.g. "Title".
				if (word[0] === word[0].toUpperCase()) {
					return capitaliseFirstLetter(token);
				}

				// Lower cased words. E.g. "test".
				return token.toLowerCase();
			}

			/**
			 * Interpolate a regexp string.
			 *
			 * @param  {[type]} str  [description]
			 * @param  {[type]} args [description]
			 * @return {[type]}      [description]
			 */
			function interpolate(str, args) {
				return str.replace(/\$(\d{1,2})/g, function (match, index) {
					return args[index] || '';
				});
			}

			var pluralizationRules = [
				[/s?$/i, 's'],
				[/([^aeiou]ese)$/i, '$1'],
				[/(ax|test)is$/i, '$1es'],
				[/(alias|[^aou]us|tlas|gas|ris)$/i, '$1es'],
				[/(e[mn]u)s?$/i, '$1s'],
				[/([^l]ias|[aeiou]las|[emjzr]as|[iu]am)$/i, '$1'],
				[/(alumn|alg|vertebr)(?:a|ae)$/i, '$1ae'],
				[/(seraph|cherub)(?:im)?$/i, '$1im'],
				[/(her|at|gr)o$/i, '$1oes'],
				[/sis$/i, 'ses'],
				[/(?:(i)fe|(ar|l|ea|eo|oa|hoo)f)$/i, '$1$2ves'],
				[/([^aeiouy]|qu)y$/i, '$1ies'],
				[/([^ch][ieo][ln])ey$/i, '$1ies'],
				[/(x|ch|ss|sh|zz)$/i, '$1es'],
				[/(matr|cod|mur|sil|vert|ind|append)(?:ix|ex)$/i, '$1ices'],
				[/eaux$/i, '$0'],
				[/m[ae]n$/i, 'men']
			];
			var len = pluralizationRules.length;

			while (len--) {
				var rule = pluralizationRules[len];

				if (rule[0].test(word)) {
					return word.replace(rule[0], function (match, index, word) {
						var result = interpolate(rule[1], arguments);

						if (match === '') {
							return restoreCase(word[index - 1], result);
						}

						return restoreCase(match, result);
					});
				}
			}
			return word;
		}

		function closeTo(num1, num2, tolerance) {
			var diff = num1 - num2;
			return Math.abs(diff) <= tolerance;
		}

		function ordinalIndicator(number) {
			var ends = ['th', 'st', 'nd', 'rd', 'th', 'th', 'th', 'th', 'th', 'th'];
			if ((number % 100) >= 11 && (number % 100) <= 13) {
				return number + 'th';
			} else {
				return number + ends[number % 10];
			}
		}

		function identity(value) {
			return value;
		}

		/**
		 * Creates a new object and maps properties from the object given according to options specified.
		 * @param {object} obj Object to be mapped
		 * @param {object} options Mapping options.
		 *		E.g. {"old_property_name": "newPropertyName", "oldComplexObject": { "old_c_obj_prop": "newComplexObjectProperty" }}
		 * @param {object} transforms Object with mapping functions which transforms values. Note: keys should represent old properties, not a new ones.
		 *		E.g. {"old_property_name": function(val) {return val.toString();} }
		 * @returns {object} New copy of the object with renamed properties
		 */
		function mapObject(obj, options, transforms) {
			var result = {};
			for (var property in obj) {
				if (!obj.hasOwnProperty(property)) {
					continue;
				}
				if (obj[property] != null && $.isPlainObject(obj[property])) {
					result[property] = mapObject(obj[property], options && options[property], transforms && transforms[property]);
				} else {
					var transformFunction = transforms && transforms[property] || identity;
					if (options && options[property]) {
						result[options[property]] = transformFunction(obj[property]);
					} else {
						result[property] = transformFunction(obj[property]);
					}
				}
			}

			return result;
		}

		/**
		 * @see mapObject for details
		 * Maps an array of objects.
		 */
		function mapObjects(objects, options, transforms) {
			var result = objects.map(function (obj) {
				var newObject = mapObject(obj, options, transforms);
				return newObject;
			});

			return result;
		}

		function capitaliseFirstLetter(string) {
			var result = string.charAt(0).toUpperCase() + string.slice(1);
			return result;
		}

		var formatting = {
			toIntString: toIntString,
			toFractionalString: toFractionalString,
			toSignedFractionalString: toSignedFractionalString,
			toIntFractionalCurrency: toIntFractionalCurrency,
			toIntCurrency: toIntCurrency,
			toFractionalCurrency: toFractionalCurrency,
			toLimitedFractionalCurrency: toLimitedFractionalCurrency,
			toPercentage: toPercentage,
			toExtendedChange: toExtendedChange,
			aOrAn: aOrAn,
			toFormattedVolume: toFormattedVolume,
			almostEqual: almostEqual,
			roundNumber: roundNumber,
			roundToMultiplier: roundToMultiplier,
			customFormat: customFormat,
			format: format,
			formatStr: formatStr,
			pluralize: pluralize,
			closeTo: closeTo,
			ordinalIndicator: ordinalIndicator,
			mapObject: mapObject,
			mapObjects: mapObjects,
			capitaliseFirstLetter: capitaliseFirstLetter,
			toNaString: toNaString,

			// functions to work with dates
			throwTimezoneOffset: throwTimezoneOffset,
			formatDate: formatDate,
			formatDateWithWeekNumber: formatDateWithWeekNumber,
			toUtcIsoDate: toUtcIsoDate,
			convertStringToUtcIsoDate: convertStringToUtcIsoDate,
			toExpiryKey: toExpiryKey,
			daysFromNow: daysFromNow,
			sameDate: areDatesEqual,
			compareDates: compareOnlyDatePartFromDates,
			resolveDate: resolveDate,
			resolveExpiry: resolveExpiry,
			getCurrentDate: getCurrentDate,
			getCurrentDateTime: getCurrentDateTime,
			getDateFromDaysInFuture: getDateFromDaysInFuture,
			parseSungardExpiry: parseSungardExpiry
		};

		return formatting;
	});