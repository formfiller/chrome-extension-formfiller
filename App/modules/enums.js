define([],function () {
	var self = this;

	this.Commands = Object.freeze({
		GET_DATA: 'getData',
		RESTORE_DATA: 'restoreData',
		GET_MAPPING: 'getMapping'
	});

	this.MappingStatuses = Object.freeze({
		ACTIVE: '1',
		SKIPPED: '2'
	});

	return self;
});
