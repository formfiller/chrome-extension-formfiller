define([], function () {
	var self = this;

	this.post = function (command, data, callback) {
		chrome.tabs.query({active: true, currentWindow: true}, function (tabs) {
			var tab = tabs[0];
			chrome.tabs.sendRequest(tab.id, {
				command: command,
				data: data
			}, function handler(response) {
				callback(response, tab);
			});
		});
	};

	return self;
});
