define(['jquery', 'noty'], function($){
	var self = this;

	var SUCCESS_TIMEOUT = 1000;
	var THEME = 'bootstrapTheme';
	var MAX_VISIBLE_COUNT = 3;

	this.success = function(text, selector){
		if(selector){
			var $element = $(selector);
			$element.noty({layout: 'top', theme: THEME, text: text, maxVisible: MAX_VISIBLE_COUNT,  type: 'success', timeout: SUCCESS_TIMEOUT ? SUCCESS_TIMEOUT : 0})
		} else {
			noty({layout: 'top', theme: THEME, text: text, maxVisible: MAX_VISIBLE_COUNT,  type: 'success', timeout: SUCCESS_TIMEOUT ? SUCCESS_TIMEOUT : 0});
		}
	};

	this.warning = function(text, selector){
		if(selector){
			var $element = $(selector);
			$element.noty({layout: 'top', theme: THEME, text: text, maxVisible: MAX_VISIBLE_COUNT,  type: 'warning', timeout: SUCCESS_TIMEOUT ? SUCCESS_TIMEOUT : 0})
		} else {
			noty({layout: 'top', theme: THEME, text: text, maxVisible: MAX_VISIBLE_COUNT,  type: 'warning', timeout: SUCCESS_TIMEOUT ? SUCCESS_TIMEOUT : 0});
		}
	};

	return self;
});