define([],function () {
	var self = this;

	this.defaultElementComparer = function(first, second){
		return (first.id && second.id && first.id === second.id)
			|| (first.name && second.name && first.name === second.name)
			|| (!first.id && !second.id && !first.name && !second.name);
	};

	this.defaultElementIdentifier = function(item){
		return item.id || item.name;
	};

	this.removeDuplicated = function(array, compare){
		// duplicated elements
		var duplicated = [];
		// unique elements
		var unique = [];

		compare = compare || defaultElementComparer;

		for (var i = 0; i < array.length; i++) {
			for (var j = i + 1; j < array.length; j++) {
				if (compare(array[i], array[j])) {
					duplicated.push(array[i]);
				}
			}
		}

		unique = array.filter(function (item) {
			if ((item.id && duplicated.filter(function (val) {
					return val.id === item.id;
				}).length > 1)
				|| (item.name && duplicated.filter(function (val) {
					return val.name === item.name;
				}).length > 1)) {
				return false;
			}
			return true;
		});

		return unique;
	};

	this.removeNotIdentified = function(array, identifier){
		identifier = identifier || defaultElementIdentifier;

		var elementsWithIdentifier = array.filter(function (item) {
			var identifierInfo = identifier(item);
			return identifierInfo;
		});

		return elementsWithIdentifier;
	};

	return self;
});
